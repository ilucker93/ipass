# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


admin_role = Role.find_or_create_by(name: 'admin')
Role.find_or_create_by(name: 'user')


user = User.create(email: 'admin@admin.com', password: 'adminadmin', password_confirmation: 'adminadmin', role: admin_role)

RuRate.create( title: 'Минимальный:', card_limit: 'До 100 карточек в месяц', card_type: '1 тип карточки')
RuRate.create( title: 'Оптимальный:', card_limit: 'До 1000 карточек в месяц', card_type: '3 типа карточкек')
RuRate.create( title: 'Максимальный:', card_limit: 'До 10000 карточек в месяц', card_type: '6 типов карточкек')

UaRate.create( title: 'Мінімальний::', card_limit: 'До 100 карток на місяць', card_type: '1 тип картки')
UaRate.create( title: 'Оптимальний:', card_limit: 'До 1000 карток на місяць', card_type: '3 типи карток')
UaRate.create( title: 'Максимальний:', card_limit: 'До 10000 карток на місяць', card_type: '6 типів карток')

EnRate.create( title: 'Minimum:', card_limit: 'Up to 100 cards per month', card_type: '1 card type')
EnRate.create( title: 'Optimal:', card_limit: 'Up 1000 cards per month', card_type: '3 types of cards')
EnRate.create( title: 'Maximum:', card_limit: 'To 10000 cards per month', card_type: '6 types of cards')
