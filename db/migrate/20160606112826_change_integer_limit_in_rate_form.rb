class ChangeIntegerLimitInRateForm < ActiveRecord::Migration
  def change
    change_column :rate_forms, :phone, :integer, limit: 8
  end 
end
