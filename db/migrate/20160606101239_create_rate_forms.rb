class CreateRateForms < ActiveRecord::Migration
  def change
    create_table :rate_forms do |t|
      t.string :name
      t.string :mail
      t.string :comment
      t.string :ratename
      t.integer :phone, :limit => 8

      t.timestamps null: false
    end
  end
end
