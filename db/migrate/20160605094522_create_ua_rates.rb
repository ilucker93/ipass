class CreateUaRates < ActiveRecord::Migration
  def change
    create_table :ua_rates do |t|
      t.string :title
      t.string :card_limit
      t.string :card_type

      t.timestamps null: false
    end
  end
end
