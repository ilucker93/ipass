class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :nickname
      t.string :mail
      t.string :theme
      t.string :text

      t.timestamps null: false
    end
  end
end
