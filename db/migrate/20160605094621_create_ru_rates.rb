class CreateRuRates < ActiveRecord::Migration
  def change
    create_table :ru_rates do |t|
      t.string :title
      t.string :card_limit
      t.string :card_type

      t.timestamps null: false
    end
  end
end
