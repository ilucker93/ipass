require 'test_helper'

class UaControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get solution" do
    get :solution
    assert_response :success
  end

  test "should get rates" do
    get :rates
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

end
