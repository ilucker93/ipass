        class RuController < ApplicationController
          def index
            @contact = Contact.new
            if @contact.save
              redirect_to :root
              ContactsMailer.sample_email(@contact).deliver_now
            end
          end

          def solution
            @contact = Contact.new
            if @contact.save
              redirect_to :root
              ContactsMailer.sample_email(@contact).deliver_now
            end
          end

          def rates
            @rurates = RuRate.all
            @contact = Contact.new
            if @contact.save
              redirect_to :root
              ContactsMailer.sample_email(@contact).deliver_now
            end
          end

          def contact
            @contact = Contact.new
            if @contact.save
              redirect_to :root
              ContactsMailer.sample_email(@contact).deliver_now
            end
          end
        end
