class ContactsController < ApplicationController

    def index
    @contacts = Contact.all
    end

    def new
    @contact = Contact.new
  end


  def show
    @contact = Contact.find(params[:id])
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to :root
      ContactsMailer.sample_email(@contact).deliver_now
    end
  end


  private
  def contact_params 
    params.require(:contact).permit(:nickname, :mail, :theme, :text)
  end

end