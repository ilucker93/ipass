class RateFormController < ApplicationController
  def new
  	@rateform = RateForm.new
  end

  def create
    @rateform = RateForm.new(rate_params)
    if @rateform.save
      redirect_to :root
      RateFormMailer.rate_email(@rateform).deliver_now
    end
  end

  def encreate
     @rateform = RateForm.new
      RateFormMailer.rate_email(@rateform).deliver_now
     # ContactsMailer.sample_email(@contact).deliver_now
  end

  def rucreate
     @rateform = RateForm.new
      RateFormMailer.rate_email(@rateform).deliver_now
     # ContactsMailer.sample_email(@contact).deliver_now
  end



  private
  def rate_params 
    params.require(:rate_form).permit(:name, :mail, :comment, :ratename, :phone)
  end

end
