class RateFormMailer < ApplicationMailer

	default from: "ipass.com.ua@gmail.com"
				#template_path: 'contacts_mailer/sample_email'

		def rate_email(rate_form) # название вьюхи маилера и аргумент 
			@rateform = rate_form
	    mail(to: 'ipass.com.ua@gmail.com' , subject: 'Rate form info')
	  end
end
