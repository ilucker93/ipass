// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require animate-css
//= require es5-shim.min
//= require html5shiv-printshiv.min
//= require html5shiv.min
//= require PageScroll2id.min
//= require plugins-scroll
//= require respond.min
//= require_tree .

$(function() {

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	}

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});

var ready;
ready = function() {

	if (!$("div").is("#main_head")) {
		$("#header").addClass("blue_header");
	}


	function heightDetect() {
		if ($(window).width() < 800) {
			if ($(window).width() < 680 && $(window).width() > 400) {
				$("#main_head").css("height", $(window).height()*0.8);
			} else {
				$("#main_head").css("height", $(window).height()*0.5);
			}
		} else {
			$("#main_head").css("height", $(window).height()*0.8);
		}
	}

	heightDetect();
	dushedContainerHeight();

	$(window).resize(function() {
		heightDetect();
		var res = true;
		dushedContainerHeight(res);
		if (!$(".nav_button").is(":visible")) {
			$(".mobile_navigation").fadeOut();
		}
	});

	function dushedContainerHeight() {
		var maxH = 0;
		$(".dashed_container").each(function (res) {
			if(res) {
				$(this).height("");
			}
			var h_block = parseInt($(this).height());
			if(h_block > maxH) {
				maxH = h_block;
			}
		});
		$(".dashed_container").height(maxH);
	}

	$(".navigation").clone().appendTo(".mobile_navigation");

	$(".nav_button").on("click", function() {
		if ($(".mobile_navigation").is(":visible")) {
			$(".mobile_navigation").fadeOut();
		} else {
			$(".mobile_navigation .navigation").css("display", "block");
			$(".mobile_navigation").fadeIn();
		}
	});

	$(".voprosi").on("click", function() {
		if ($(".voprosi ul li").is(":visible")) {
			$(".voprosi ul li").hide(300);
			if (!$(this).children().children().is(":visible")){
				$(this).children().children().show(300);
			}
		} else {
			$(this).children().children().show(300);
		}
	});

	$("#scroller").mPageScroll2id({scrollSpeed: 900});
};

$(document).ready(ready);
$(document).on('page:load', ready);
