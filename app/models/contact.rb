class Contact < ActiveRecord::Base

	belongs_to :ua
	belongs_to :ru
	belongs_to :en

	validates :nickname,  presence: true 
	validates :mail,  format: { with:  /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i }
	validates :theme, presence: true 
  	validates :text , presence: true 

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  #def headers
   ###### end
end
